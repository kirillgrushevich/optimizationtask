using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    [SerializeField] private Sprite[] iconsSmall;
    [SerializeField] private Sprite[] iconsBig;


    private void Start()
    {
        foreach (var sprite in iconsSmall)
        {
            var btn = Instantiate(GameObject.Find("SmallIconButton").GetComponent<Button>(), GameObject.Find("SmallIconButton").transform.parent);
            btn.image.sprite = sprite;
            btn.onClick.AddListener(delegate
            {
                SetupPlayerIcon(sprite.name);
            });
        }
        
        SetupPlayerIcon(iconsSmall[0].name);
        
        GameObject.Find("SmallIconButton").gameObject.SetActive(false);
    }

    private void SetupPlayerIcon(string iconName)
    {
        GameObject.Find("PlayerIconImage").GetComponent<Image>().sprite =
            iconsBig.FirstOrDefault(i => i.name == iconName);

    }
}
