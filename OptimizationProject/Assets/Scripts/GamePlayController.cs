using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[Serializable]
public class ActionInfo
{
    public bool isEmotion;
    public Emotion emotion;
    public MovementAction movementAction;
    public Sprite sprite;
}

public class GamePlayController : MonoBehaviour
{
    [SerializeField] private ActionInfo[] actions;
    [SerializeField] private float waitingTime;
    [SerializeField] private float showingTime;

    private float timer;
    private float targetTimer;
    
    private ActionInfo currentAction;

    private Vector3 cameraOffset;

    private int score;

    private void Start()
    {
        var player = FindObjectOfType<PlayerCharacter>();
        var mainCamera = Camera.main;

        cameraOffset = mainCamera.transform.position - player.transform.position;

        targetTimer = waitingTime;
        
        var actionImage = GameObject.Find("ActionImageUiLabel").GetComponent<Image>();
        actionImage.transform.localScale = Vector3.zero;
        
        gameObject.SetActive(false);
    }

    private void Update()
    {
        Debug.Log("Start update");
        var updateTimer = Time.realtimeSinceStartup;
        timer += Time.deltaTime;
        if (timer < targetTimer)
        {
            Debug.Log("Wait for showing");
            return;
        }

        if (currentAction == null)
        {
            var actionTimer = Time.realtimeSinceStartup;
            Debug.Log("Started setup current action");
            currentAction = actions[Random.Range(0, actions.Length)];
            
            GameObject.Find("ActionImageUiLabel").GetComponent<Image>().sprite = currentAction.sprite;
            GameObject.Find("ActionImageUiLabel").GetComponent<Image>().SetNativeSize();
            GameObject.Find("ActionImageUiLabel").GetComponent<Image>().color = new Color(1, 1, 1, 0f);
            GameObject.Find("ActionImageUiLabel").GetComponent<Image>().DOColor(Color.white, 0.3f);
            GameObject.Find("ActionImageUiLabel").transform.localScale = Vector3.zero;
            GameObject.Find("ActionImageUiLabel").transform.DOScale(Vector3.one, 0.3f);
            
            targetTimer = showingTime;
            timer = 0f;
            Debug.Log($"Completed setup current action {Time.realtimeSinceStartup - actionTimer}");
            return;
        }

        Debug.Log("Setup label");
        GameObject.Find("ActionImageUiLabel").GetComponent<Image>().DOColor(new Color(1, 1, 1, 0f), 0.3f);
        GameObject.Find("ActionImageUiLabel").transform.DOScale(Vector3.zero, 0.3f);

        if (currentAction.isEmotion)
        {
            if (FindObjectOfType<PlayerCharacter>().CurrentEmotion == currentAction.emotion)
            {
                Debug.Log("Up Score");
                score++;
                GameObject.Find("CFX_Magical_Source").GetComponent<ParticleSystem>().Play();
                FindObjectOfType<SoundController>().PlayWin();
            }
            else
            {
                Debug.Log("Down Score");
                score--;
                FindObjectOfType<SoundController>().PlayLose();
            }
        }
        else
        {
            if (FindObjectOfType<PlayerCharacter>().CurrentMovementAction == currentAction.movementAction)
            {
                Debug.Log("Up Score");
                score++;
                GameObject.Find("CFX_Magical_Source").GetComponent<ParticleSystem>().Play();
                FindObjectOfType<SoundController>().PlayWin();
            }
            else
            {
                Debug.Log("Down Score");
                score--;
                FindObjectOfType<SoundController>().PlayLose();
            }
        }

        Debug.Log("Setup Score");
        GameObject.Find("GameScoreUILabel").GetComponent<Text>().text = $"Score: {score}";
        targetTimer = waitingTime;
        timer = 0f;
        currentAction = null;
        
        Debug.Log($"Complete update {Time.realtimeSinceStartup - updateTimer}");
    }

    private void LateUpdate()
    {
        Debug.Log("Setup camera position");
        var player = FindObjectOfType<PlayerCharacter>();
        var mainCamera = Camera.main;

        mainCamera.transform.position = player.transform.position + cameraOffset;
    }
}