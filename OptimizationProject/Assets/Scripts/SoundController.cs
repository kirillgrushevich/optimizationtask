using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class SoundController : MonoBehaviour
{
    [SerializeField] private AudioClip[] musicClips;
    [SerializeField] private AudioClip[] winClips;
    [SerializeField] private AudioClip[] looseClips;

    private AudioSource musicSource;
    private AudioSource resultSource;

    public void PlayWin()
    {
        resultSource.clip = winClips[Random.Range(0, winClips.Length)];
        resultSource.Play();
    }

    public void PlayLose()
    {
        resultSource.clip = looseClips[Random.Range(0, looseClips.Length)];
        resultSource.Play();
    }
    
    private void Start()
    {
        musicSource = gameObject.AddComponent<AudioSource>();
        musicSource.volume = 0.5f;
        musicSource.loop = false;

        resultSource = gameObject.AddComponent<AudioSource>();
        
        musicSource.clip = musicClips[Random.Range(0, musicClips.Length)];
        musicSource.Play();
    }

    private void Update()
    {
        if (musicSource.isPlaying)
        {
            return;
        }
        
        musicSource.clip = musicClips[Random.Range(0, musicClips.Length)];
        musicSource.Play();
    }
}