public enum Emotion
{
    Default,
    Smile,
    Surprise,
    Frown,
    Wink,
    Cry,
    Mean,
}

public enum MovementAction
{
    Stay,
    Walk,
    Run,
    Attack,
}