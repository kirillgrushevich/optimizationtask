using System;
using UnityEngine;


public class PlayerCharacter : MonoBehaviour
{
    public Emotion CurrentEmotion => currentEmotion;
    public MovementAction CurrentMovementAction => currentMovementAction;

    private Emotion currentEmotion;
    private MovementAction currentMovementAction;

    private float speed;


    public void SetEmotion(int emotion)
    {
        currentEmotion = (Emotion)emotion;
    }

    public void SetMovement(int movement)
    {
        currentMovementAction = (MovementAction)movement;

        var movementType = (MovementAction)movement;
        speed = movementType switch
        {
            MovementAction.Stay => 0f,
            MovementAction.Walk => 1f,
            MovementAction.Run => 2f,
            MovementAction.Attack => 0f,
            _ => throw new ArgumentOutOfRangeException()
        };
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
    }
}
