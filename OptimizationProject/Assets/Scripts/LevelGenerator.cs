using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private Material[] floorMaterials;

    private float floorDeadLine;
    private float wallDeadLine;
    private Material floorMaterial;

    private void Start()
    {
        floorMaterial = floorMaterials[Random.Range(0, floorMaterials.Length)];
        
        GameObject.Find("BaseFloorElement").transform.position = new Vector3(-1000, -1000, -1000);
        
        GenerateRoomFloor(6f);
        GenerateRoomFloor(0f);
        GenerateRoomFloor(-6f);
        GenerateRoomFloor(-12f);
        
        GenerateWall(-10f);
        GenerateWall(-30f);

        floorDeadLine = -6f;
        wallDeadLine = -30f;
    }

    private void Update()
    {
        if (GameObject.Find("Barbarian").transform.position.z < floorDeadLine)
        {
            GenerateFloor();
        }
        
        if (GameObject.Find("Barbarian").transform.position.z < wallDeadLine)
        {
            GenerateWall(wallDeadLine - 20f);
            wallDeadLine -= 20f;
        }
    }

    private void GenerateWall(float z)
    {
        var obj = Instantiate(GameObject.Find("BaseWallElement"), 
            new Vector3(3f, 0f, z), Quaternion.identity);
    }

    private void GenerateFloor()
    {
        GameObject.Find("BaseFloorElement").transform.position = new Vector3(-1000, -1000, -1000);
        GenerateRoomFloor(floorDeadLine - 12f);
        floorDeadLine -= 6f;
    }

    private void GenerateRoomFloor(float centerZ)
    {
        for (var x = -2; x <= 2; x += 2)
        {
            for (var z = -2; z <= 2; z += 2)
            {
                var obj = Instantiate(GameObject.Find("BaseFloorElement"), 
                        new Vector3(x, 0f, centerZ + z), Quaternion.identity);

                obj.GetComponent<MeshRenderer>().sharedMaterial = Instantiate(floorMaterial);
            }
        }
    }
}